import React, { Component } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native';
import { Container, Header, Title, Button, Left, Body, Icon, } from 'native-base';
import { sty } from './styles';
import imgLogo from '../../images/ios_icon.png';
import imgQR from '../../images/QR.png';

const styles = StyleSheet.create({

    icon: {
      color: 'red',
      fontSize: 16,
      paddingTop: 20,
      paddingLeft: 40,
    },
  });
  
class about extends Component {
  
  render() {
    return (
    <Container style={sty.container}>   
    <Image 
              style={sty.logoImg}
              source={imgLogo}
              />
       
      <View style={sty.body}>
        
        <Icon ios='ios-call' android="md-call" style={styles.icon}>    
        <Text style={sty.textAbout}>  093-542-5554  , 045-711-531 </Text></Icon>

        <Icon ios='ios-link' android="md-link" style={styles.icon}>    
        <Text style={sty.textAbout}>  www.picoyasothon.com </Text></Icon>

        <Icon ios='logo-facebook' android="logo-facebook" style={styles.icon}>    
        <Text style={sty.textAbout}>  https://www.facebook.com/picoyasothon</Text></Icon>

        <Icon ios='ios-chatbubbles' android="md-chatbubbles" style={styles.icon}>    
        <Text style={sty.textAbout}>  Line ID: @voi0649v (มี @ นำหน้าด้วย) </Text></Icon>

        <Icon ios='ios-pin' android="md-pin" style={styles.icon}>    
        <Text style={sty.textAbout}>  79/1 หมู่ 9 ถ.แจ้งสนิท ต.เขื่องคำ อ.เมืองยโสธร จ.ยโสธร 35000 (ห่างจากโรงพยาบาล-->แม็คโครยโสธร ประมาณ 800 เมตร)</Text></Icon>

      </View> 
      <Image 
            style={sty.ImgQR} 
            source={imgQR} 
            />  
    </Container>
    );
  }

}


export default about;
