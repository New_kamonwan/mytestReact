import { StyleSheet } from 'react-native';
import metrics from '../../configs/metrics';
import material from '../../configs/material';

const {
    LOGO_WIDTH,
    LOGO_HEIGHT,
    FONT_SIZE_SMALL,
    FONT_SIZE_MEDIUM,
    FONT_SIZE_LARGE,
    DEVICE_HEIGHT,   
    DEVICE_WIDTH,
    QR_HEIGHT,
    QR_WIDTH
 
    } = metrics

    export const sty = StyleSheet.create({
        container: {
            flexDirection: 'column',
            width: metrics.DEVICE_WIDTH,
            height: metrics.DEVICE_HEIGHT,
            paddingTop: '12%',
            backgroundColor: material.backgroundColorgray
        },
        body: { flex: 1.5, alignSelf: 'center', backgroundColor: material.backgroundColorgray,  marginTop: '-10%'},
        logoImg: {
            flex: 1,
            height: null,
            width: LOGO_WIDTH,
            alignSelf: 'center',
            resizeMode: 'contain',

        },
        ImgQR: {
            flex: 1,
            height: null,
            width: QR_WIDTH,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            resizeMode: 'contain',
            marginTop: '-20%',

        },

        textAbout: { fontSize: FONT_SIZE_LARGE,  color: material.fontGray },

    })
    