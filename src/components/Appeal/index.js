import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TextInput, ScrollView, Platform } from 'react-native';
import { Container, Header, Title, Button, Left, Body, Icon, Card, CardItem, InputGroup, Input } from 'native-base';
import { sty } from './style';
import img from '../../images/support.png';
import material from '../../configs/material';

const styles = StyleSheet.create({
    icon: {
      color: 'red',
      fontSize: 16,
      paddingTop: 20,
      paddingLeft: 50,
    },
    platform: {
      backgroundColor: Platform.OS === 'ios' ? 'red' : '#fff',
    },  
  });


class appeal extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     text: 'ส่งเรื่องร้องเรียน',
  //   };
  // }
  render() {
    return (
      <Container style={sty.container}>   
        <Card>
            <CardItem>
              <Body style={{ flexDirection: 'row' }}>
                <Image 
                    style={sty.Img}
                    source={img}
                />
                <View style={sty.texthader}>
                  <Text>ชื่อผู้ร้องเรียน......</Text>
                  <Text> วันที่...... , เวลา......</Text>
                  <Text>สัญญาเลขที่........</Text>
                </View>
              </Body>
            </CardItem>
          </Card> 

          <Text style={sty.text}> รายละเอียด </Text>
        
        <View
            style={sty.body}
        >
                    <InputGroup borderType="regular">
                        <Input  
                        editable
                        maxLength={250}
                        multiline
                        placeholder="เรื่องร้องเรียน"
                        numberOfLines={4}
                         onChangeText={(text) => this.setState({ text })}
                
                        />
                    </InputGroup>

     </View>
          <Button  style={sty.buttonShort}>
            <Text style={[sty.textAppeal, { color: material.fontWhite }]}> ส่งเรื่องร้องเรียน  </Text>
          </Button>         
    </Container>
    );
  }
}


export default appeal;
