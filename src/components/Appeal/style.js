import { StyleSheet } from 'react-native';
import metrics from '../../configs/metrics';
import material from '../../configs/material';

const {
    IMG_WIDTH,
    IMG_HEIGHT,
    FONT_SIZE_SMALL,
    FONT_SIZE_MEDIUM,
    FONT_SIZE_LARGE,
    DEVICE_HEIGHT,   
    DEVICE_WIDTH,
    QR_HEIGHT,
    QR_WIDTH,
    ITEM_HEIGHT,
    ITEM_WIDTH
    } = metrics

    export const sty = StyleSheet.create({
        container: {
            flexDirection: 'column',
            width: metrics.DEVICE_WIDTH,
            height: metrics.DEVICE_HEIGHT,
            paddingTop: '12%',
            backgroundColor: material.backgroundColorgray
        },
        body: { flex: 2, 
                alignSelf: 'flex-start', 
                backgroundColor: material.gray1,  
                marginTop: '2%',
                marginLeft: '2%',
                paddingRight: '5%',
                width: '96%',
                borderBottomColor: '#E8E8E8',
                borderColor: '#E8E8E8',
                borderWidth: 2,
                flexWrap: 'wrap'
             },
        Img: {
            height: IMG_HEIGHT,
            width: IMG_WIDTH,
            alignSelf: 'flex-start',
            resizeMode: 'contain',
            marginLeft: '5%',


        },
        ImgQR: {
            flex: 1,
            height: null,
            width: QR_WIDTH,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            resizeMode: 'contain',
            marginTop: '-20%',

        },
        buttonShort: {
            alignItems: 'center',
            alignSelf: 'center',
            justifyContent: 'center',
            backgroundColor: material.red,
            height: ITEM_HEIGHT,
            width: ITEM_WIDTH,
            borderRadius: 10,
            resizeMode: 'contain',
            marginTop: '10%',
            marginBottom: '10%'
            
        },
        text: {
            fontSize: FONT_SIZE_LARGE,
            color: material.gray4, 
            alignSelf: 'center',
            justifyContent: 'center',
            marginTop: '5%',
        },
        texthader: {
            fontSize: FONT_SIZE_LARGE,
            color: material.gray4, 
            justifyContent: 'space-between',
            marginTop: '5%',
            marginLeft: '5%'
        },
        textAppeal: { fontSize: FONT_SIZE_LARGE,  color: material.fontWhite,  },

    })
    