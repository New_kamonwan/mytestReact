export default {

    gray1: '#E8E8E8',
    gray2: '#B5B5B5',
    gray3: '#696969',
    gray4: '#343F4B',
    red: '#FC3B3F',
    blue1: '#2E669D',
    blue2: '#29487d',
    blue3: '#174D8D',

    fontWhite: 'white',
    fontGray: '#343F4B',
    fontBule: '#174D8D',

    backgroundColorWhite: 'white',
    backgroundColorgray: '#f9f9f9',
    borderBottomB: '#000000'
}