import { Dimensions, Platform } from 'react-native'

const IS_ANDROID = Platform.OS === 'android'
const { height, width } = Dimensions.get('window')

const ratioX = value => width * value
const ratioY = value => height * value

export default {
    ANDROID_STATUSBAR: 24,
    DEVICE_HEIGHT: IS_ANDROID ? height - 24 : height,
    DEVICE_WIDTH: width,

    FONT_SIZE_SMALL: ratioY(0.016),
    FONT_SIZE_MEDIUM: ratioY(0.018),
    FONT_SIZE_LARGE: ratioY(0.02),

    /* about in page  */
    LOGO_WIDTH: ratioX(0.46),
    LOGO_HEIGHT: ratioY(0.28),
    /* imageQR */
    QR_WIDTH: ratioX(0.50),
    QR_HEIGHT: ratioY(0.15),

    /* appeal in page  */
    IMG_WIDTH: ratioX(0.26),
    IMG_HEIGHT: ratioY(0.18),

    /* end sign in page   */
    IMAGE_HEIGHT: width / 2,
    IMAGE_HEIGHT_SMALL: width / 7,
   /* button appeal in page   */
    ITEM_HEIGHT: ratioY(0.066),
    ITEM_WIDTH: ratioX(0.94),
}
