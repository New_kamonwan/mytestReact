import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native';
import { Container, Header, Title,  Button, Left, Body, Icon} from 'native-base';

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#fff',
      paddingTop : 25,
    },
    title: {
      alignSelf:'center',
      fontSize:20,
      justifyContent:'center',
      alignItems:'center',
      color: '#000',
    },
     
  });
  
class App extends Component {
  
  render() {
    return (
      <View style ={styles.container}>
        <Header  style={{ backgroundColor: '#f9f9f9' }}>
          <Body>
            <Title style={styles.title} >ติดต่อเรา</Title>
          </Body>
        </Header>
      </View>
    );
  }
}

export default App;
