import React from 'react';
import App from './src/app';
import About from './src/components/about/index';
import Appeal from './src/components/Appeal/index'
import { Spinner, Container } from 'native-base';
import { Asset, AppLoading } from 'expo';

export default class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      isReady: false 
    };
  }

  async componentWillMount() {
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
    await Expo.Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
    });
    this.setState({ isReady: true });
  }
  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.setState({ isReady: true })}
          onError={console.warn}
        />
        // <Container style={{ backgroundColor: '#fff', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
        //   <Spinner />
        // </Container>

    );
  }
   return (
      <Appeal />
      
    );
 }
}


